Welcome to our app!

Since we integrated this application into another application (IBA) about 3/4ths of the way through our project, this snapshot of our application still contains a fair amount of bugs including, but not limited to
	1. Not being able to open documents properly since we used classes that a different team developed for that
	2. Only asking for permissions upon entering and not checking if the user accepted those permissions upon the user performing an action that requires such permissions
	3. Crashing upon leaving the app before an asynchronous task finishes

Additionally changes that are not necessarily bugs were implemented in the new app
	1. The settings drawer no longer exists, it was moved into a separate module of the app
	2. The styling was updated to accommodate IBA

For further documentation on the following topics, see the associated section in the "FinalDocumentation" document

Topic                  Section in FinalDocumentation
1. Using the App       1. User Guide
2. Deploying the App   2. Deployment Guide
3. Developing the App  3. Development Guide

NOTE: The "FinalDocumentation.pdf" is for the final version of our application found within IBA, so some of the information may not apply to the code found here