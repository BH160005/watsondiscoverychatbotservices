package edutechnologic.nlds.vmac.WatBot;

import android.support.test.rule.ActivityTestRule;
import android.widget.EditText;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

import java.util.ArrayList;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.instanceOf;

/**
 * Created by rachelmarks on 4/10/18.
 */

public class DiscoveryResponseTest {
    @Rule
    public ActivityTestRule<MainActivity> activityRule =
            new ActivityTestRule(MainActivity.class);


    @Test
    public void shouldDisplayDatabase() {

        onView(allOf(withId(R.id.message),instanceOf(EditText.class))).perform(clearText(), typeText("What is a database"));
        onView(withId(R.id.btn_send)).perform(click());
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //check response of chatbot
        ArrayList<Message> chat = MainFragment.messageArrayList;
        Message bot = chat.get(chat.size()-1);
        String msg = bot.getMessage();
        String comparedMsg = "A database is an organized collection of data. Typically, databases are managed by using a database-management system (DBMS), which is a computer-software application that interacts with end-users, other applications, and the database itself to capture and analyze data. A general-purpose DBMS allows the definition, creation, querying, update, and administration of databases.";
        Assert.assertEquals(msg, comparedMsg);

    }
}