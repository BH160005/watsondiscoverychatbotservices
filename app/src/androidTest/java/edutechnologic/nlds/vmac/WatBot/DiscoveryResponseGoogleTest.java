package edutechnologic.nlds.vmac.WatBot;

import android.support.test.rule.ActivityTestRule;
import android.widget.EditText;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.instanceOf;

/**
 * Created by rachelmarks on 4/10/18.
 */

public class DiscoveryResponseGoogleTest {
    @Rule
    public ActivityTestRule<MainActivity> activityRule =
            new ActivityTestRule(MainActivity.class);


    @Test
    public void shouldDisplayDatabase() {
        onView(allOf(withId(R.id.message),instanceOf(EditText.class))).perform(clearText(), typeText("search: google"));
        onView(withId(R.id.btn_send)).perform(click());
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //check response of chatbot
        onView(withText("paper.dvi")).check(matches(withId(R.id.text)));
    }
}
