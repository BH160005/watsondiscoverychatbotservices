package edutechnologic.nlds.vmac.WatBot;

import android.support.test.rule.ActivityTestRule;
import android.widget.EditText;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.instanceOf;


/**
 * Created by rachelmarks on 4/2/18.
 */

public class SettingsFontIncreaseTest {
    @Rule
    public ActivityTestRule<MainActivity> activityRule =
            new ActivityTestRule(MainActivity.class);




    @Test
    public void shouldDisplayDatabase() {
        int original = Settings.message_font_size;
        onView(allOf(withId(R.id.message),instanceOf(EditText.class))).perform(clearText(), typeText("increase font size"));

        onView(withId(R.id.btn_send)).perform(click());
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //increases from font size 24
         int level = Settings.message_font_size;
         Assert.assertEquals(level, original + 7);
    }

}
