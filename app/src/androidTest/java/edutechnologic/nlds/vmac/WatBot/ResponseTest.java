package edutechnologic.nlds.vmac.WatBot;

import android.support.test.espresso.contrib.DrawerActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.Gravity;
import android.widget.EditText;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static android.support.test.espresso.Espresso.onView;

import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeRight;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withResourceName;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.instanceOf;

/**
 * Created by rachelmarks on 3/25/18.
 */
@RunWith(AndroidJUnit4.class)
public class ResponseTest {

    @Rule
    public ActivityTestRule<MainActivity> activityRule =
            new ActivityTestRule(MainActivity.class);

/*types datatbase which will result in response of rephrasing due to key words for search*/


    @Test
    public void shouldDisplayDatabase() {
        MainFragment.messageArrayList.clear();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(allOf(withId(R.id.message),instanceOf(EditText.class))).perform(clearText(), typeText("Database"));

        onView(withId(R.id.btn_send)).perform(click());
       try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
       //check response of chatbot
        ArrayList<Message> chat = MainFragment.messageArrayList;
        Message bot = chat.get(chat.size()-1);
        String msg = bot.getMessage();
        String comparedMsg = "I didn't understand. You can try rephrasing.";
        Assert.assertEquals(msg, comparedMsg);

    }


}
