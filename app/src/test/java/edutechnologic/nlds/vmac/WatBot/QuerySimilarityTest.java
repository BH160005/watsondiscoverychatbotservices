package edutechnologic.nlds.vmac.WatBot;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class QuerySimilarityTest {

    @Test
    public void similarQueries() throws Exception {
        String query1 = "Red foxes love brown dogs";
        String query2 = "Red foxes love dogs";
        double expectedSimilarity = 0.8944;

        /*
        query1 -> [1, 1, 1, 1, 1]
        query2 -> [1, 1, 1, 0, 1]
        [1, 1, 1, 1, 1]*[1, 1, 1, 0, 1] = 4
        magnitude of vector 1 = sqrt(5)
        magnitude of vector 2 = sqrt(4) = 2
        cosine similarity = 4 / (sqrt(5)*2) = 0.8944
        */

        double similarity = Comparison.cosineSimilarity(query1, query2);

        assertEquals(expectedSimilarity, similarity , 0.001);
    }

    @Test
    public void asimilarQueries() throws Exception {
        String query1 = "Red foxes love brown dogs";
        String query2 = "The world turns quickly";
        double expectedSimilarity = 0.0;

        double similarity = Comparison.cosineSimilarity(query1, query2);

        assertEquals(expectedSimilarity, similarity, 0.001);
    }

    @Test
    public void identicalQueries() throws Exception {
        String query = "Red foxes love brown dogs";
        double expectedSimilarity = 1.0;

        double similarity = Comparison.cosineSimilarity(query, query);

        assertEquals(expectedSimilarity, similarity, 0.001);
    }

    @Test
    public void somewhatSimilarQueries() throws Exception {
        String query1 = "Red foxes love brown dogs";
        String query2 = "Brown dogs hate parks";
        double expectedSimilarity = 0.4472;

        /*
        query1 -> [1, 1, 1, 1, 1, 0, 0]
        query2 -> [0, 0, 0, 1, 1, 1, 1]
        [1, 1, 1, 1, 1, 0, 0]*[0, 0, 0, 1, 1, 1, 1] = 2
        magnitude of vector 1 = sqrt(5)
        magnitude of vector 2 = sqrt(4) = 2
        cosine similarity = 2 / (sqrt(5)*2) = 0.4472
        */

        double similarity = Comparison.cosineSimilarity(query1, query2);

        assertEquals(expectedSimilarity, similarity, 0.001);
    }
}