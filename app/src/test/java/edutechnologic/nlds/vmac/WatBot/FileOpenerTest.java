package edutechnologic.nlds.vmac.WatBot;

import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

/**
 * Created by seannemann on 2/8/2018.
 */

public class FileOpenerTest {

    FileOpener fileOpener;
    @Before
    public void runBeforeTestMethod() {
        fileOpener = new FileOpener();
    }

    @Test
    public void mimeTypeOfTextFile() throws Exception {
        File fileName = new File("file://car.txt");
        assertEquals(fileOpener.getMimeTypeOfFile(fileName), "text/plain");
    }

    @Test
    public void mimeTypeOfPdfFile() throws Exception {
        File fileName = new File("file://bird.pdf");
        assertEquals(fileOpener.getMimeTypeOfFile(fileName), "application/pdf");
    }

    @Test
    public void mimeTypeOfWordFile() throws Exception {
        File fileName = new File("file://party/words.doc");
        assertEquals(fileOpener.getMimeTypeOfFile(fileName), "application/msword");
    }

    @Test
    public void mimeTypeOfNoFile() throws Exception {
        File fileName = new File("");
        assertEquals(fileOpener.getMimeTypeOfFile(fileName), "*/*");
    }

    @Test
    public void mimeTypeOfNoExtension() throws Exception {
        File fileName = new File("happy");
        assertEquals(fileOpener.getMimeTypeOfFile(fileName), "*/*");
    }
}
