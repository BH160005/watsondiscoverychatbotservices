package edutechnologic.nlds.vmac.WatBot;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;

import java.io.File;
import java.io.FileOutputStream;

/**
 * The WebViewActivity displays given HTML in a webview if file is not downloaded
 * @author Allison Galuska
 * @since 2017-09-17
 */
public class WebViewActivity extends AppCompatActivity {

    WebView webView;
    EditText text;
    DiscoveryResult dResult;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        webView = (WebView) findViewById(R.id.webview);
        text = (EditText) findViewById(R.id.url);

        Button button = (Button) findViewById (R.id.run);
        text.setVisibility(View.GONE);
        button.setVisibility(View.GONE);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                goToPage(text.getText().toString());
            }
        });

        Button buttonDownload = (Button) findViewById (R.id.download);
        buttonDownload.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                saveFile();
            }
        });

        if(getIntent().hasExtra("result"))
            dResult = (DiscoveryResult) getIntent().getSerializableExtra("result");
        if(getIntent().hasExtra(Constants.Intents.HTML))
            goToPage(getIntent().getStringExtra(Constants.Intents.HTML));
        else if(getIntent().hasExtra(Constants.Intents.URL))
            goToUrl(getIntent().getStringExtra(Constants.Intents.URL));
    }

    private void saveFile(){
        //create file
        File folderPath = new File(Environment.getExternalStorageDirectory(), "Download");
        //rename it with an html at the end since json is formatted as html
        String filename = dResult.getExtracted_metadata().getFilename();
        filename = filename.substring(0, filename.indexOf('.'));
        filename += ".html";
        File file = new File(folderPath, filename);
        FileOutputStream outputStream;

        try {
            // write file
            outputStream = new FileOutputStream(file);
            outputStream.write(dResult.getHtml().getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    private void goToPage(String html){

        webView.setWebViewClient(new Callback());
        webView.loadData(html, "text/html; charset=utf-8", "UTF-8");

    }

    @SuppressLint("SetJavaScriptEnabled")
    private void goToUrl(String url){
        webView.setWebViewClient(new Callback());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webView.loadUrl(url);
    }

    private class Callback extends WebViewClient{

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return (false);
        }

    }

}