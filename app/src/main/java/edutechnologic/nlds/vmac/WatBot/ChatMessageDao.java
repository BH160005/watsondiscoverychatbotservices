package edutechnologic.nlds.vmac.WatBot;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by Liz on 3/31/2018.
 */

@Dao
public interface ChatMessageDao {
    @Query("SELECT * FROM chatmessage")
    List<ChatMessage> getAll();

    @Query("SELECT * FROM chatmessage ORDER BY uid")
    List<ChatMessage> getAllMessagesById();

    @Insert
    void insertAll(ChatMessage... chatMessages);

    @Delete
    void delete(ChatMessage chatMessage);

    @Query("DELETE FROM chatmessage")
    public void nukeTable();

}
