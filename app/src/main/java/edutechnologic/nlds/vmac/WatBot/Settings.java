package edutechnologic.nlds.vmac.WatBot;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.design.widget.NavigationView;
import android.view.View;
import android.widget.SeekBar;
import android.widget.Switch;

/**
 * Created by seannemann on 3/22/2018.
 */

public class Settings {

    public static int message_font_size;
    private int messageFontLevel;
    private int minimum_font_size = 10;
    private int zoom_scale = 7;
    private int max_font_level = 10;
    private  int min_font_level = 0;
    private SharedPreferences sharedPrefs;
    private ChatAdapter chatAdapter;
    private boolean autoTts;
    private View view;
    SeekBar sk;
    Switch autoTtsSwitch;

    Settings(SharedPreferences sPrefs, ChatAdapter cAdapter, SeekBar textSeek, Switch s)
    {
        sharedPrefs = sPrefs;
        chatAdapter = cAdapter;
        sk = textSeek;
        autoTtsSwitch = s;
        messageFontLevel = sharedPrefs.getInt("pref_font_level", 1);
        message_font_size = messageFontLevel * zoom_scale + minimum_font_size;
        cAdapter.notifyDataSetChanged();
        DashboardFragment.updateAdapter();

        autoTts = sharedPrefs.getBoolean("pref_auto_tts_readbackss", false);
        setupTextSizeSeekBar();
        initializeAutoTtsSwitch();
    }

    public int getMessageFontLevel() {
        return messageFontLevel;
    }

    public boolean updateFontLevel(int newLevel) {
        return changeFontLevel(newLevel);
    }

    public boolean incrementFontLevel() {
        return changeFontLevel(messageFontLevel + 1);
    }

    public boolean decrementFontLevel() {
        return changeFontLevel(messageFontLevel - 1);
    }

    public boolean getAutoTts()
    {
        return autoTts;
    }

    public boolean toggleAutoTts()
    {
        autoTts = !autoTts;
        autoTtsSwitch.setChecked(autoTts);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putBoolean("pref_auto_tts_readbackss", autoTts);
        return editor.commit();
    }


    private boolean changeFontLevel(int newLevel)
    {
        if(newLevel < min_font_level || newLevel > max_font_level) return false;
        messageFontLevel = newLevel;
        sk.setProgress(messageFontLevel);
        message_font_size = messageFontLevel * zoom_scale + minimum_font_size;
        chatAdapter.notifyDataSetChanged();
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putInt("pref_font_level", newLevel);
        DashboardFragment.updateAdapter();

        return editor.commit();
    }

    private void setupTextSizeSeekBar() {

        sk.setProgress(messageFontLevel);
        sk.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
                updateFontLevel(progress);
            }
        });
    }

    private void initializeAutoTtsSwitch() {

        //set switch equal to value in stored preferences
        autoTtsSwitch.setChecked(autoTts);
        //set up listener to handle auto text to speech toggle
        autoTtsSwitch.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                toggleAutoTts();
            }
        });
    }
}
