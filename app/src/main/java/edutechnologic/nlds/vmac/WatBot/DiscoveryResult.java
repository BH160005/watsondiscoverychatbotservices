package edutechnologic.nlds.vmac.WatBot;

import java.io.Serializable;

/**
 * Result object returned by Discovery
 *
 * @author Don Herwig
 * @since 2017-10-17
 */

public class DiscoveryResult implements Serializable {

    private DiscoveryExtractedMetadata extracted_metadata;
    private String html;
    private String text;

    /**
     * Gets Metadata from Discovery
     *
     * @return DiscoveryEtractedMetadata - object containing metadata information
     */
    public DiscoveryExtractedMetadata getExtracted_metadata() {
        return extracted_metadata;
    }

    /**
     * Sets extracted metadata
     *
     * @param extracted_metadata metadata from discovery
     */
    public void setExtracted_metadata(DiscoveryExtractedMetadata extracted_metadata) {
        this.extracted_metadata = extracted_metadata;
    }

    /**
     * Gets HTML from Discovery
     *
     * @return String - HTML document
     */
    public String getHtml() {
        return html;
    }

    /**
     * Sets HTML
     *
     * @param html HTML of document
     */
    public void setHtml(String html) {
        this.html = html;
    }

    /**
     * Gets text of document
     *
     * @return String - containing text of document
     */
    public String getText() {
        return text;
    }

    /**
     * Sets text of document
     *
     * @param text text of document
     */
    public void setText(String text) {
        this.text = text;
    }
}
