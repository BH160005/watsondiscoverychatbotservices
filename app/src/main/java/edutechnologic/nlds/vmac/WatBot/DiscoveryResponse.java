package edutechnologic.nlds.vmac.WatBot;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Response object returned by Discovery
 *
 * @author Don Herwig
 * @since 2017-10-17
 */
public class DiscoveryResponse implements Serializable {

    private ArrayList<DiscoveryResult> results;

    /**
     * Gets results from discovery response
     *
     * @return ArrayList<DiscoveryResult> - results as an array of DiscoveryResult objects
     * @see DiscoveryResult
     */
    public ArrayList<DiscoveryResult> getResults() {
        return results;
    }

    /**
     * Sets array with Results
     *
     * @param results An array of Discovery Result objects containing results
     * @see DiscoveryResult
     */
    public void setResults(ArrayList<DiscoveryResult> results) {
        this.results = results;
    }
}
