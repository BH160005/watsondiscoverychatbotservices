package edutechnologic.nlds.vmac.WatBot;

import java.util.Iterator;
import java.util.List;

/**
 * Created by Liz on 2/8/2018.
 */

public class Cache{

    /*
    Params:
    providedQuery: he user's query, which is to be compared with queries in the cache to determine if there is
    a similar query whose response could be used
    similarityThreshold: the minimum similarity acceptable

    This method iterates through the cache and finds the query that is most similar to the provided
    query, and returns the stored response. Only returns query responses whose query similarity is =
    above the provided threshold. Otherwise returns null.
     */
    public static WatsonResponse getResponseFromMostSimilarQuery(AppDatabase db, String providedQuery, double similarityThreshold){
        List<WatsonResponse> allResponses = db.mWatsonResponseDaoDao().getAll();
        Iterator it = allResponses.iterator();
        double bestSimilarity = similarityThreshold;
        WatsonResponse mostSimilarResponse = null;
        while (it.hasNext()) {
            WatsonResponse response = (WatsonResponse) it.next();
            double similarity = Comparison.cosineSimilarity(response.getUser_query(), providedQuery);
            if(similarity > bestSimilarity){
                bestSimilarity = similarity;
                mostSimilarResponse = response;
            }
            it.remove(); // avoids a ConcurrentModificationException
        }
        return mostSimilarResponse;
    }
}
