package edutechnologic.nlds.vmac.WatBot;

/**
 * Created by Don on 10/9/2017.
 */

public class Constants {

    public static class Intents{
        public static String RANK_AND_RETRIEVAL_RESULTS = "RANK_AND_RETRIEVAL_RESULTS";
        public static String DISCOVERY_RESULTS = "DISCOVERY_RESULTS";
        public static String HTML = "HTML";
        public static String URL = "URL";
    }

    public static class RankAndRetrieval{
        public static final String USERNAME = "15118da6-091b-43a0-925e-402fe0ac413a";
        public static final String PASSWORD = "XEMg6UhkaibV";
        public static final String SOLR_CLUSTER_ID = "sc4578a22f_f386_4b53_be0e_9778500fda95";
        public static final String COLLECTION_NAME = "chem_stackexchange_collection";
        public static final String FORM = "json";
        public static final String FIELDS = "id, title, url";
    }

    public static class Discovery{
        public static final String USERNAME = "ccd891c6-382b-41eb-9061-a14fdcb6dd6f";
        public static final String PASSWORD = "tdIjMw4fhQRQ";
        public static final String ENVIRONMENT_ID = "5699926e-af1e-4cd7-a732-74650d0f49b1";
        public static final String COLLECTION_ID = "42cd888c-f029-4847-bd5a-db18f31c2b51";
        public static final String VERSION = "2017-09-01";
        public static final int COUNT = 5;
    }

    public static class Conversation{
        public static final String USERNAME = "1bf5e320-be9e-44f0-a923-4e1dab8574cc";
        public static final String PASSWORD = "MuwJE0WEQred";
        public static final String WORKSPACE_ID = "783aa2cc-9e34-4197-a730-700f7ea16e06";
        public static final String VERSION = "2017-05-26";
    }

    public static class Permissions{
        public static final int RECORD_AUDIO = 500;
    }
}
