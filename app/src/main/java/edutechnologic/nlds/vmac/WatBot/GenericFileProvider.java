package edutechnologic.nlds.vmac.WatBot;

import android.support.v4.content.FileProvider;

/**
 * Created by seannemann on 2/7/2018.
 */

/***
 * Class exists essentially to rename FileProvider so our Android Manifest file does not conflict
 * with a third party libraries Android Manifest file
 */
public class GenericFileProvider extends FileProvider {}