package edutechnologic.nlds.vmac.WatBot;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

/**
 * Created by Liz on 2/22/2018.
 */

@Database(entities = {WatsonResponse.class, ChatMessage.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {
    public abstract WatsonResponseDao mWatsonResponseDaoDao();
    public abstract ChatMessageDao mChatMessageDao();
}