package edutechnologic.nlds.vmac.WatBot;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.FileProvider;

import java.io.File;
import java.io.IOException;

/**
 * Created by seannemann on 2/8/2018.
 */

public class FileOpener {

    /***
     * Sends broadcast intent to open file in outside application
     *
     * @param context Context from which to open file
     * @param file File to open
     */
    public void openFile(Context context, File file) throws IOException {

        Uri uri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", file);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(uri, getMimeTypeOfFile(file));
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        context.startActivity(intent);
    }

    /***
     * Retrieves mime type corresponding to file
     *
     * @param file File for which to check mime type
     * @return Mime type of file
     */
    public String getMimeTypeOfFile(File file)
    {
        String url = file.toString();
        String mimeType = "*/*";
        // Check what kind of file you are trying to open, by comparing the url with extensions
        if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
            // Word document
             mimeType = "application/msword";
        } else if(url.toString().contains(".html")) {
            // html file
            mimeType = "text/html";
        } else if(url.toString().contains(".pdf")) {
            // PDF file
            mimeType = "application/pdf";
        } else if(url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
            // Powerpoint file
            mimeType = "application/vnd.ms-powerpoint";
        } else if(url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
            // Excel file
            mimeType = "application/vnd.ms-excel";
        } else if(url.toString().contains(".zip") || url.toString().contains(".rar")) {
            // WAV audio file
            mimeType = "application/x-wav";
        } else if(url.toString().contains(".rtf")) {
            // RTF file
            mimeType = "application/rtf";
        } else if(url.toString().contains(".wav") || url.toString().contains(".mp3")) {
            // WAV audio file
            mimeType = "audio/x-wav";
        } else if(url.toString().contains(".gif")) {
            // GIF file
            mimeType = "image/gif";
        } else if(url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
            // JPG file
            mimeType = "image/jpeg";
        } else if(url.toString().contains(".txt")) {
            // Text file
            mimeType = "text/plain";
        } else if(url.toString().contains(".3gp") || url.toString().contains(".mpg") || url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
            // Video files
            mimeType = "video/*";
        }

        return mimeType;
    }

}
