package edutechnologic.nlds.vmac.WatBot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Liz on 2/6/2018.
 */

public class Comparison {

    //obtained from http://xpo6.com/list-of-english-stop-words/
    private static Set<String> stopWords = new HashSet<>(Arrays.asList("a", "about", "above", "above", "across", "after", "afterwards", "again", "against", "all", "almost", "alone", "along", "already", "also","although","always","am","among", "amongst", "amoungst", "amount",  "an", "and", "another", "any","anyhow","anyone","anything","anyway", "anywhere", "are", "around", "as",  "at", "back","be","became", "because","become","becomes", "becoming", "been", "before", "beforehand", "behind", "being", "below", "beside", "besides", "between", "beyond", "bill", "both", "bottom","but", "by", "call", "can", "cannot", "cant", "co", "con", "could", "couldnt", "cry", "de", "describe", "detail", "do", "done", "down", "due", "during", "each", "eg", "eight", "either", "eleven","else", "elsewhere", "empty", "enough", "etc", "even", "ever", "every", "everyone", "everything", "everywhere", "except", "few", "fifteen", "fify", "fill", "find", "fire", "first", "five", "for", "former", "formerly", "forty", "found", "four", "from", "front", "full", "further", "get", "give", "go", "had", "has", "hasnt", "have", "he", "hence", "her", "here", "hereafter", "hereby", "herein", "hereupon", "hers", "herself", "him", "himself", "his", "how", "however", "hundred", "ie", "if", "in", "inc", "indeed", "interest", "into", "is", "it", "its", "itself", "keep", "last", "latter", "latterly", "least", "less", "ltd", "made", "many", "may", "me", "meanwhile", "might", "mill", "mine", "more", "moreover", "most", "mostly", "move", "much", "must", "my", "myself", "name", "namely", "neither", "never", "nevertheless", "next", "nine", "no", "nobody", "none", "noone", "nor", "not", "nothing", "now", "nowhere", "of", "off", "often", "on", "once", "one", "only", "onto", "or", "other", "others", "otherwise", "our", "ours", "ourselves", "out", "over", "own","part", "per", "perhaps", "please", "put", "rather", "re", "same", "see", "seem", "seemed", "seeming", "seems", "serious", "several", "she", "should", "show", "side", "since", "sincere", "six", "sixty", "so", "some", "somehow", "someone", "something", "sometime", "sometimes", "somewhere", "still", "such", "system", "take", "ten", "than", "that", "the", "their", "them", "themselves", "then", "thence", "there", "thereafter", "thereby", "therefore", "therein", "thereupon", "these", "they", "thickv", "thin", "third", "this", "those", "though", "three", "through", "throughout", "thru", "thus", "to", "together", "too", "top", "toward", "towards", "twelve", "twenty", "two", "un", "under", "until", "up", "upon", "us", "very", "via", "was", "we", "well", "were", "what", "whatever", "when", "whence", "whenever", "where", "whereafter", "whereas", "whereby", "wherein", "whereupon", "wherever", "whether", "which", "while", "whither", "who", "whoever", "whole", "whom", "whose", "why", "will", "with", "within", "without", "would", "yet", "you", "your", "yours", "yourself", "yourselves", "the"));

    //calculates cosine similarity between two strings using the bag of words model
    public static double cosineSimilarity(String str1, String str2){
        double similarity = 0.0;

        //use regular expression for words to split string into an array of words
        String[] words1 = str1.split("\\W+");
        String[] words2 = str2.split("\\W+");

        List<String> totalWordList = new ArrayList<String>();

        //add each word from the first string to the list of total words, as long as it's not a stop word
        for(int index = 0; index < words1.length; index ++) {
            String word = words1[index].toLowerCase();
            if(!stopWords.contains(word) && !totalWordList.contains(word)) {
                totalWordList.add(word);
            }
        }
        //add each word from the first string to the list of total words, as long as it's not a stop word
        for(int index = 0; index < words2.length; index ++) {
            String word = words2[index].toLowerCase();
            if(!stopWords.contains(word) && !totalWordList.contains(word)) {
                totalWordList.add(word);
            }
        }

        int vectorLength = totalWordList.size();

        //initialize vector for each of the two original strings (filled with zeros by default)
        int vector1[] = new int[vectorLength];
        int vector2[] = new int[vectorLength];

        //increment the word vector for each instance of the word in the string
        for(int index = 0; index < words1.length; index++) {
            String word = words1[index].toLowerCase();
            int wordLoc = totalWordList.indexOf(word);
            //make sure the word is in the totalWord list (i.e. not a stop word)
            if(wordLoc != -1) {
                vector1[wordLoc]++;
            }
        }

        for(int index = 0; index < words2.length; index++) {
            String word = words2[index].toLowerCase();
            int wordLoc = totalWordList.indexOf(word);
            //make sure the word is in the totalWord list (i.e. not a stop word)
            if(wordLoc != -1) {
                vector2[wordLoc]++;
            }
        }

        //compute cosine similarity
        double dotProd = dotProduct(vector1, vector2);
        double magnitude1 = vectorMagnitude(vector1);
        double magnitude2 = vectorMagnitude(vector2);
        double magnitudeProduct = magnitude1 * magnitude2;
        if(magnitudeProduct > 0) {
            similarity = dotProd / magnitudeProduct;
        }

        return similarity;
    }

    //requires the two arrays be of the same length
    private static int dotProduct(int[] arr1, int[] arr2) {
        int dotProd = 0;
        for(int index = 0; index < arr1.length; index++) {
            dotProd = dotProd + (arr1[index]*arr2[index]);
        }
        return dotProd;
    }

    private static double vectorMagnitude(int[] arr) {
        double magnitude = 0.0;
        for(int index = 0; index < arr.length; index++) {
            magnitude = magnitude + arr[index]*arr[index];
        }
        return Math.sqrt(magnitude);
    }
}