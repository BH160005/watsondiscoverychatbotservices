package edutechnologic.nlds.vmac.WatBot;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by seannemann on 2/20/2018.
 */

public class NetworkUtil {

    Context context;

     public NetworkUtil(Context c)
     {
         context = c;
     }

    /**
     * Check Internet Connection
     *
     * @return Whether or not a connection to the internet exists
     */
    public boolean checkInternetConnection() {
        // get Connectivity Manager object to check connection
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        // Check for network connections
        if (isConnected) {
            return true;
        } else {
            return false;
        }

    }

}
