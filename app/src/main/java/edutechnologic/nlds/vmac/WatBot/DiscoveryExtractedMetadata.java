package edutechnologic.nlds.vmac.WatBot;

import java.io.Serializable;

/**
 * Metadata object returned by Discovery
 *
 * @author Don Herwig
 * @since 2017-10-17
 */
public class DiscoveryExtractedMetadata implements Serializable {

    private String title;
    private String file_type;
    private String filename;

    /**
     * Retrieves the title of the Discovery Document
     *
     * @return String - title of document
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title of the document
     *
     * @param title title of Discovery document
     */
    public void setTitle(String title) {
        this.title = title;
    }

    public String getFile_type() {
        return file_type;
    }

    public void setFile_type(String file_type) {
        this.file_type = file_type;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
