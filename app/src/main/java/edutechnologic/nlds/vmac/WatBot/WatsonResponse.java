package edutechnologic.nlds.vmac.WatBot;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Liz on 2/22/2018.
 */

@Entity
class WatsonResponse {
    @PrimaryKey(autoGenerate = true)
    private int uid;

    @ColumnInfo(name = "user_query")
    private String user_query;

    @ColumnInfo(name = "conversation_response")
    private String conversation_response;

    @ColumnInfo(name = "document_text")
    private String document_text;

    @ColumnInfo(name = "html")
    private String html;

    @ColumnInfo(name = "file_name")
    private String file_name;

    @ColumnInfo(name = "title")
    private String title;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getUser_query(){
        return user_query;
    }

    public void setUser_query(String user_query) {
        this.user_query = user_query;
    }

    public String getConversation_response() {
        return conversation_response;
    }

    public void setConversation_response(String conversation_response) {
        this.conversation_response = conversation_response;
    }

    public String getDocument_text() {
        return document_text;
    }

    public void setDocument_text(String document_text) {
        this.document_text = document_text;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}