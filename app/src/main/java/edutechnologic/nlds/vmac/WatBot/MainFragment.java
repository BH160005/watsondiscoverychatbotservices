package edutechnologic.nlds.vmac.WatBot;

import android.Manifest;
import android.app.DownloadManager;
import android.arch.persistence.room.Room;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.ibm.mobilefirstplatform.clientsdk.android.analytics.api.Analytics;
import com.ibm.mobilefirstplatform.clientsdk.android.core.api.BMSClient;
import com.ibm.mobilefirstplatform.clientsdk.android.core.api.Response;
import com.ibm.mobilefirstplatform.clientsdk.android.core.api.ResponseListener;
import com.ibm.mobilefirstplatform.clientsdk.android.logger.api.Logger;
import com.ibm.watson.developer_cloud.android.library.audio.MicrophoneHelper;
import com.ibm.watson.developer_cloud.android.library.audio.MicrophoneInputStream;
import com.ibm.watson.developer_cloud.android.library.audio.utils.ContentType;
import com.ibm.watson.developer_cloud.conversation.v1.ConversationService;
import com.ibm.watson.developer_cloud.conversation.v1.model.MessageRequest;
import com.ibm.watson.developer_cloud.conversation.v1.model.MessageResponse;
import com.ibm.watson.developer_cloud.discovery.v1.Discovery;
import com.ibm.watson.developer_cloud.discovery.v1.model.collection.GetCollectionsRequest;
import com.ibm.watson.developer_cloud.discovery.v1.model.collection.GetCollectionsResponse;
import com.ibm.watson.developer_cloud.discovery.v1.model.environment.GetEnvironmentsRequest;
import com.ibm.watson.developer_cloud.discovery.v1.model.environment.GetEnvironmentsResponse;
import com.ibm.watson.developer_cloud.discovery.v1.model.query.QueryRequest;
import com.ibm.watson.developer_cloud.discovery.v1.model.query.QueryResponse;
import com.ibm.watson.developer_cloud.speech_to_text.v1.model.RecognizeOptions;
import com.ibm.watson.developer_cloud.speech_to_text.v1.model.SpeechResults;
import com.ibm.watson.developer_cloud.speech_to_text.v1.websocket.RecognizeCallback;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import edutechnologic.nlds.vmac.WatBot.R;

import static android.app.Activity.RESULT_OK;


public class MainFragment extends Fragment implements android.speech.tts.TextToSpeech.OnInitListener {


    private RecyclerView recyclerView;
    private ChatAdapter mAdapter;
    public static ArrayList messageArrayList;
    private EditText inputMessage;
    private ImageButton btnSend;
    private ImageButton btnRecord;
    private Map<String, Object> context = new HashMap<>();
    private boolean initialRequest;
    private boolean permissionToRecordAccepted = false;
    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
    private static String TAG = "MainActivity";
    private static final int RECORD_REQUEST_CODE = 101;
    private boolean listening = false;
    private STT speechService;
    private MicrophoneInputStream capture;
    private Logger myLogger;
    private Context mContext;
    private String workspace_id;
    private String conversation_username;
    private String conversation_password;
    private String STT_username;
    private String STT_password;
    private String analytics_APIKEY;
    private SpeakerLabelsDiarization.RecoTokens recoTokens;
    private MicrophoneHelper microphoneHelper;
    private String environmentId;
    private String collectionId ;
    private String discovery_username;
    private String discovery_password;
    private String discovery_version;
    private String discovery_api_endpoint;
    private final int PERMISSION_ALL = 1234;
    private final String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private Cache cache = new Cache();
    private final double SIMILARITY_THRESHOLD = 0.5;
    private final String DATABASE_NAME = "discovery-cache";
    private AppDatabase db;
    private String mostRecentQuery;
    private String mostRecentResponse;
    private NetworkUtil networkUtil;
    TTS textToSpeech;
    Settings settings;
    private final int REQ_CODE_SPEECH_INPUT = 100;
    private SpeechRecognizer sr;
    View v;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_main, container, false);

        FragmentManager fm = getChildFragmentManager();
        DashboardFragment dash = (DashboardFragment) fm.findFragmentById(R.id.dashboard_fragment);
        if (dash == null) {
            dash = new DashboardFragment();
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.dashboard, dash);
            ft.commit();
            fm.executePendingTransactions();
        }

        mContext = getContext();

        SetupWatsonCredentials();

        // setup utility class to check network connection
        networkUtil = new NetworkUtil(getContext());

        // text to speech wrapper for both online and offline
        textToSpeech = new TTS(getContext());


        // database
        db = Room.databaseBuilder(getContext(), AppDatabase.class, DATABASE_NAME).allowMainThreadQueries().build();

        myLogger = Logger.getLogger("myLogger");
        setupBluemixAnalytics();

        setupButtonListeners();

        inputMessage = (EditText) v.findViewById(R.id.message);

        String customFont = "Montserrat-Regular.ttf";
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), customFont);
        inputMessage.setTypeface(typeface);

        microphoneHelper = new MicrophoneHelper(getActivity());

        sr = SpeechRecognizer.createSpeechRecognizer(getContext());
        sr.setRecognitionListener(new MainFragment.speechListener());

        if (!hasPermissions(getContext(), PERMISSIONS)) {
            ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);
        }

        messageArrayList = new ArrayList<>();
        mAdapter = new ChatAdapter(messageArrayList);
        setupChatRecyclerView();
        populateChatWithMessagesFromCache();

        final SeekBar sk=(v.findViewById(R.id.nav_view)).findViewById(R.id.textSizeSeek);
        final Switch s =(v.findViewById(R.id.nav_view)).findViewById(R.id.auto_tts_response);
        final Button clearCacheButton = (v.findViewById(R.id.nav_view)).findViewById(R.id.clear_cache_button);
        final Button clearConversationHistoryButton = (v.findViewById(R.id.nav_view)).findViewById(R.id.clear_conversation_history);
        settings = new Settings(getActivity().getPreferences(Context.MODE_PRIVATE), mAdapter, sk, s);
        inputMessage.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                public boolean onEditorAction(TextView v, int actionId,
                                              KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE
                            || (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))) {
                        messageSendPressedOrEnter();
                    }
                    return false;
                }
            });

        clearCacheButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db.mWatsonResponseDaoDao().nukeTable();
                Toast.makeText(getActivity(), "Cache cleared", Toast.LENGTH_SHORT).show();
            }
        });

        clearConversationHistoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db.mChatMessageDao().nukeTable();
                messageArrayList.clear();
                mAdapter.notifyDataSetChanged();
                Toast.makeText(getActivity(), "Conversation history cleared", Toast.LENGTH_SHORT).show();
            }
        });

        // initialize Watson Conversation
        sendMessageToConversation("");

        return v;
    }

    private void populateChatWithMessagesFromCache(){
        List<ChatMessage> chatList = db.mChatMessageDao().getAllMessagesById();

        for(ChatMessage chatMessage : chatList){
            Message msg = new Message();
            msg.setMessage(chatMessage.getMessage());
            msg.setId(chatMessage.getOrigin_id().toString());
            messageArrayList.add(msg);
        }
        if(!messageArrayList.isEmpty()){
            mAdapter.notifyDataSetChanged();
        }
    }


    private void setupChatRecyclerView() {

        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        this.inputMessage.setText("");
        this.initialRequest = true;

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), recyclerView, new ClickListener() {

            @Override
            public void onClick(View view, final int position) {
                Thread thread = new Thread(new Runnable() {
                    public void run() {
                        Message audioMessage;
                        try {
                            audioMessage = (Message) messageArrayList.get(position);
                            if (audioMessage != null && !audioMessage.getMessage().isEmpty()){
                                textToSpeech.speak(audioMessage.getMessage());
                            }
                            else
                            {
                                textToSpeech.speak("No Text Specified");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                thread.start();
            }

            @Override
            public void onLongClick(View view, int position) {
                recordMessage();

            }
        }));

    }

    private String getEnvironmentId(){

        Discovery discovery = new Discovery(discovery_version);
        discovery.setEndPoint(discovery_api_endpoint);
        discovery.setUsernameAndPassword(discovery_username, discovery_password);

        GetEnvironmentsRequest getRequest = new GetEnvironmentsRequest.Builder().build();
        GetEnvironmentsResponse getResponse = discovery.getEnvironments(getRequest).execute();


        //Return first environment that isn't 'system'
        int i = 0;
        String envId = "system";
        while (envId.equals("system") && i < getResponse.getEnvironments().size()){

            envId = getResponse.getEnvironments().get(i).getEnvironmentId();
            i++;
        }

        return envId;
    }


    private String getCollectionID(){

        Discovery discovery = new Discovery(discovery_version);
        discovery.setEndPoint(discovery_api_endpoint);
        discovery.setUsernameAndPassword(discovery_username, discovery_password);



        GetCollectionsRequest getRequest = new GetCollectionsRequest.Builder(environmentId).build();
        GetCollectionsResponse getResponse = discovery.getCollections(getRequest).execute();

        //Return first (and hopefully only) collection
        return getResponse.getCollections().get(0).getCollectionId();
    }

    /**
     * Assigns values to class variables representing Watson credentials.
     * Strings are taken from config.xml file
     */
    private void SetupWatsonCredentials() {
        // Conversation Setup
        workspace_id = mContext.getString(R.string.workspace_id);
        conversation_username = mContext.getString(R.string.conversation_username);
        conversation_password = mContext.getString(R.string.conversation_password);



        // Discovery Setup
        discovery_username = mContext.getString(R.string.discovery_username);
        discovery_password = mContext.getString(R.string.discovery_password);
        discovery_version = mContext.getString(R.string.discovery_version);
        discovery_api_endpoint = mContext.getString(R.string.discovery_api_endpoint);

//        environmentId = getEnvironmentId();
//        collectionId = getCollectionID();



    }

    private void setupButtonListeners() {

        btnSend = (ImageButton) v.findViewById(R.id.btn_send);
        btnRecord = (ImageButton) v.findViewById(R.id.btn_record);
        btnSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    messageSendPressedOrEnter();
                }
            });

        btnRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recordMessage();
            }
        });
    }



    private void setupBluemixAnalytics() {
        //Bluemix Mobile Analytics
        BMSClient.getInstance().initialize(getContext(), BMSClient.REGION_US_SOUTH);
        //Analytics is configured to record lifecycle events.
        Analytics.init(getActivity().getApplication(), "WatBot", analytics_APIKEY, false, Analytics.DeviceEvent.ALL);
        //Analytics.send();
//        myLogger = Logger.getLogger("myLogger");
        // Send recorded usage analytics to the Mobile Analytics Service
        Analytics.send(new ResponseListener() {
            @Override
            public void onSuccess(Response response) {
                // Handle Analytics send success here.
            }

            @Override
            public void onFailure(Response response, Throwable throwable, JSONObject jsonObject) {
                // Handle Analytics send failure here.
            }
        });

        // Send logs to the Mobile Analytics Service
        Logger.send(new ResponseListener() {
            @Override
            public void onSuccess(Response response) {
                // Handle Logger send success here.
            }

            @Override
            public void onFailure(Response response, Throwable throwable, JSONObject jsonObject) {
                // Handle Logger send failure here.
            }
        });
    }

    private void messageSendPressedOrEnter(){
        if (!this.inputMessage.getText().toString().trim().isEmpty()) {
            String inputmessage = this.inputMessage.getText().toString().trim();
            this.inputMessage.setText("");
            this.mostRecentQuery = inputmessage;
            this.mAdapter.notifyDataSetChanged();

            // Hide the keyboard again
            View view = getActivity().getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }

            WatsonResponse response = cache.getResponseFromMostSimilarQuery(db, inputmessage, SIMILARITY_THRESHOLD);
            //if there is an internet connection
            if (networkUtil.checkInternetConnection()) {
                //if there is an acceptably similar response in the cache
                if (response != null) {
                    displayFromCache(response);
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            mAdapter.notifyDataSetChanged();
                            if (mAdapter.getItemCount() > 1) {
                                recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, mAdapter.getItemCount() - 1);

                            }
                        }
                    });
                } else {
                    sendMessageToConversation(inputmessage);
                }
            } else {
                //no internet connection so just response from most similar query in the cache
                response = cache.getResponseFromMostSimilarQuery(db, inputmessage, 0.0);
                if (response != null) {
                    displayFromCache(response);
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            mAdapter.notifyDataSetChanged();
                            if (mAdapter.getItemCount() > 1) {
                                recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, mAdapter.getItemCount() - 1);

                            }
                        }
                    });
                }
            }
        }
    }

    private void displayFromCache(WatsonResponse response){
        //Toast.makeText(MainActivity.this, response.getDocument_text(), Toast.LENGTH_LONG).show();
        addMessageToChat(response.getUser_query(), "1");
        addMessageToChat(response.getConversation_response(), "2");

        DiscoveryExtractedMetadata dem = new DiscoveryExtractedMetadata();
        dem.setFilename(response.getFile_name());
        dem.setTitle(response.getTitle());

        DiscoveryResult discoveryResult = new DiscoveryResult();
        discoveryResult.setText(response.getDocument_text());
        discoveryResult.setHtml(response.getHtml());
        discoveryResult.setExtracted_metadata(dem);

        DiscoveryResponse discoveryResponse = new DiscoveryResponse();
        ArrayList<DiscoveryResult> list = new ArrayList<>();
        list.add(discoveryResult);
        discoveryResponse.setResults(list);

        DashboardFragment dashboard = getDashboard();
        openDashboard(discoveryResponse, response.getUser_query());
    }

    /***
     * Updates and displays dashboard
     *
     * @param response Response data with which to update dashboard
     */
    private void openDashboard(final DiscoveryResponse response, final String query) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                DashboardFragment dashboard = getDashboard();
                dashboard.setResult(response);

                // pass in necessary information to be added to cache with clicked document
                dashboard.putArguments(mostRecentResponse, mostRecentQuery);

                showDashboard();
                dashboard.expandDashboard();
            }
        });
    }

    /***
     * Handler for when permissions are accepted. Currently simply logs results or displays to user
     *
     * @param requestCode The request code passed in {@link #requestPermissions(String[], int)}.
     * @param permissions The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     *     which is either {@link android.content.pm.PackageManager#PERMISSION_GRANTED}
     *     or {@link android.content.pm.PackageManager#PERMISSION_DENIED}. Never null.
     *
     * @see #requestPermissions(String[], int)
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_RECORD_AUDIO_PERMISSION:
                permissionToRecordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
            case RECORD_REQUEST_CODE: {

                if (grantResults.length == 0
                        || grantResults[0] !=
                        PackageManager.PERMISSION_GRANTED) {

                    Log.i(TAG, "Permission has been denied by user");
                } else {
                    Log.i(TAG, "Permission has been granted by user");
                }
                return;
            }

            case MicrophoneHelper.REQUEST_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(), "Permission to record audio denied", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    /***
     * Checks to see if the application context has all of the inputted permissions
     *
     * @param context context within which to check
     * @param permissions permissions to check
     * @return whether or not the application has all of the inputted permissions within the context
     */
    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    /***
     * Sends query to discovery and opens dashboard with results
     */
    private void sendMessageToDiscovery(final String query) {

        new Thread(new Runnable() {
            public void run() {
                Discovery discovery = new Discovery("2017-11-07");
                discovery.setEndPoint("https://gateway.watsonplatform.net/discovery/api/");
                discovery.setUsernameAndPassword(discovery_username, discovery_password);

                environmentId = getEnvironmentId();
                collectionId = getCollectionID();

                QueryRequest.Builder queryBuilder = new QueryRequest.Builder(environmentId, collectionId);
                queryBuilder.query(query);
                QueryResponse queryResponse = discovery.query(queryBuilder.build()).execute();
                Gson helper = new Gson();
                final DiscoveryResponse response = helper.fromJson(helper.toJson(queryResponse), DiscoveryResponse.class);

                openDashboard(response, query);
                Log.v("Discovery", queryResponse.toString());
            }
        }).start();
    }

    /***
     * Retrieves dashboard fragment
     *
     * @return Reference to dashboard fragment
     */
    private DashboardFragment getDashboard() {
        return (DashboardFragment) getChildFragmentManager().findFragmentById(R.id.dashboard);
    }

    /***
     * Displays dashboard
     */
    private void showDashboard() {
        getChildFragmentManager().beginTransaction().show(getDashboard()).commit();

//        getChildFragmentManager().beginTransaction().setCustomAnimations(R.animator.anim_slide_down_up, R.animator.anim_slide_up_down).show(getDashboard()).commit();
    }

    /***
     * Sends a message to Watson Conversation Service and handles response by adding message to display
     * and querying Watson Discovery if indicated to in the Conversation response message
     */
    private void sendMessageToConversation(String input) {
        final String inputmessage = input;
        if (!this.initialRequest) {
            addMessageToChat(inputmessage, "1");
            myLogger.info("Sending a message to Watson Conversation Service");

        } else {
            Message inputMessage = new Message();
            inputMessage.setMessage(inputmessage);
            inputMessage.setId("100");
            this.initialRequest = false;
            Toast.makeText(getContext(), "Tap on the message for Voice", Toast.LENGTH_LONG).show();

        }

        new Thread(new Runnable() {
            public void run() {
                try {

                    ConversationService service = new ConversationService(ConversationService.VERSION_DATE_2017_02_03);
                    service.setUsernameAndPassword(conversation_username, conversation_password);
                    MessageRequest newMessage = new MessageRequest.Builder().inputText(inputmessage).context(context).build();
                    MessageResponse response = service.message(workspace_id, newMessage).execute();

                    //Passing Context of last conversation
                    if (response.getContext() != null) {
                        context.clear();
                        context = response.getContext();

                    }

                    if (response != null) {
                        if (response.getOutput() != null && response.getOutput().containsKey("text")) {

                            if (context.get("settings") != null){
                                changeSettings((String)context.get("settings"));
                            }

                            if (context.get("query") != null) {
                                sendMessageToDiscovery((String) context.get("query"));
                            }
                            ArrayList responseList = (ArrayList) response.getOutput().get("text");
                            while (null != responseList && responseList.size() > 0) {
                                final String outMessageText = (String) responseList.remove(0);
                                addMessageToChat(outMessageText, "2");
                                mostRecentResponse = outMessageText;
                                new Thread(new Runnable() {
                                    public void run() {
                                        if (settings.getAutoTts())
                                            textToSpeech.speak(outMessageText);
                                    }
                                }).start();
                            }
                        }
                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                mAdapter.notifyDataSetChanged();
                                if (mAdapter.getItemCount() > 1) {
                                    recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, mAdapter.getItemCount() - 1);

                                }
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void addMessageToChat(String message, String id){

            Message outMessage = new Message();
            outMessage.setMessage(message);
            outMessage.setId(id);
            ChatMessage chatMessage = new ChatMessage();
            chatMessage.setMessage(message);
            chatMessage.setOrigin_id(Integer.parseInt(id));
            db.mChatMessageDao().insertAll(chatMessage);
            messageArrayList.add(outMessage);

    }

    /***
     * Record a message via Speech To Text
     */
    private void recordMessage() {
        //mic.setEnabled(false);
        speechService = new STT(getContext());


        if (speechService.checkInternetConnection()){
            //Use Watson STT

            if (listening != true) {
                capture = microphoneHelper.getInputStream(true);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            speechService.getWatsonService().recognizeUsingWebSocket(capture, getRecognizeOptions(), new MicrophoneRecognizeDelegate());
                        } catch (Exception e) {
                            showError(e);
                        }
                    }
                }).start();
                listening = true;
                Toast.makeText(getContext(), "Listening (Online Mode)....", Toast.LENGTH_LONG).show();

            } else {
                stopWatsonRecording();

            }
        } else {
            //Google STT
            /**
             * Showing google speech input dialog
             * */


            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                    RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
            intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                    getString(R.string.speech_prompt));
            intent.putExtra(RecognizerIntent.EXTRA_PREFER_OFFLINE, true);

            Toast.makeText(getContext(), "Listening (Offline mode)....", Toast.LENGTH_SHORT).show();
            try {
                //startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
                sr.startListening(intent);
            } catch (ActivityNotFoundException a) {
                Toast.makeText(getContext(),
                        getString(R.string.speech_not_supported),
                        Toast.LENGTH_SHORT).show();
            }

        }


    }

    private void stopWatsonRecording(){
        try {
            microphoneHelper.closeInputStream();
            listening = false;
            Toast.makeText(getActivity(), "Stopped Listening....", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Receiving speech input from Android Speech RecognizerIntent
     * */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    showMicText(result.get(0));
                }
                break;
            }

        }
    }





    /***
     * Private Methods - Speech to Text
     */
    private RecognizeOptions getRecognizeOptions() {
        return new RecognizeOptions.Builder()
                .continuous(true)
                .contentType(ContentType.OPUS.toString())
                //.model("en-UK_NarrowbandModel")
                .interimResults(true)
                .inactivityTimeout(2000)
                //TODO: Uncomment this to enable Speaker Diarization
                //.speakerLabels(true)
                .build();
    }

    @Override
    public void onInit(int i) {

    }

    //Watson Speech to Text Methods.
    private class MicrophoneRecognizeDelegate implements RecognizeCallback {
        /***
         * Displays results from speech to text on screen
         *
         * @param speechResults results from speech to text to display
         */
        @Override
        public void onTranscription(SpeechResults speechResults) {
            //TODO: Uncomment this to enable Speaker Diarization
            /*recoTokens = new SpeakerLabelsDiarization.RecoTokens();
            if(speechResults.getSpeakerLabels() !=null)
            {
                recoTokens.add(speechResults);
                Log.i("SPEECHRESULTS",speechResults.getSpeakerLabels().get(0).toString());


            }*/
            if (speechResults.getResults() != null && !speechResults.getResults().isEmpty()) {
                String text = speechResults.getResults().get(0).getAlternatives().get(0).getTranscript();
                showMicText(text);
                stopWatsonRecording();
            }
        }

        @Override
        public void onConnected() {

        }

        /***
         * Displays exception if present in speech to text and enables microphone button
         *
         * @param e exception to display
         */
        @Override
        public void onError(Exception e) {
            showError(e);
            enableMicButton();
        }

        @Override
        public void onDisconnected() {
            enableMicButton();
        }

        @Override
        public void onInactivityTimeout(RuntimeException runtimeException) {

        }

        @Override
        public void onListening() {

        }

        @Override
        public void onTranscriptionComplete() {

        }
    }

    //methods for Google STT
    private class speechListener implements RecognitionListener
    {
        public void onReadyForSpeech(Bundle params)
        {
            Log.d(TAG, "onReadyForSpeech");
        }
        public void onBeginningOfSpeech()
        {
            Log.d(TAG, "onBeginningOfSpeech");
        }
        public void onRmsChanged(float rmsdB)
        {
            Log.d(TAG, "onRmsChanged");
        }
        public void onBufferReceived(byte[] buffer)
        {
            Log.d(TAG, "onBufferReceived");
        }
        public void onEndOfSpeech()
        {
            Log.d(TAG, "onEndofSpeech");
        }
        public void onError(int error)
        {
            Log.d(TAG,  "error " +  error);
            //mText.setText("error " + error);
        }
        public void onResults(Bundle results)
        {
            String str = new String();
            Log.d(TAG, "onResults " + results);
            ArrayList data = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            for (int i = 0; i < data.size(); i++)
            {
                Log.d(TAG, "result " + data.get(i));
                str += data.get(i);
            }
            if (null != data) {

                showMicText(str);
            }
        }
        public void onPartialResults(Bundle partialResults)
        {
            Log.d(TAG, "onPartialResults");
        }
        public void onEvent(int eventType, Bundle params)
        {
            Log.d(TAG, "onEvent " + eventType);
        }
    }

    /***
     * Updates text in edit text for entering user messages. Used for displaying text from speech
     * to text
     *
     * @param text Text to update
     */
    private void showMicText(final String text) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                inputMessage.setText(text);

            }
        });


    }

    /***
     * Enables microphone for speech to text
     */
    private void enableMicButton() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                btnRecord.setEnabled(true);
            }
        });
    }

    /***
     * Shows exception in console and in toast
     *
     * @param e exception to be logged
     */
    private void showError(final Exception e) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        });
    }


    /**
     * Requests download from Download manager (download service for Android GingerBread and up)
     *
     * @param url - URL of file to download
     */
    private void downloadFromURL(String url){
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));

        String filename = URLUtil.guessFileName(url, null, null);

        request.setDescription(url);
        request.setTitle(filename);

        // in order for this if to run, you must use the android 3.2 to compile your app
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, filename);

        // get download service and enqueue file
        DownloadManager manager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }

    /**
     * Modifies settings based on command
     *
     * @param  command - string corresponding to some command to modify settings; corresponds to some Watson Conversation entity
     *
     */
    private void changeSettings(String command) {

        //Final for use in inner class
        final String cmd = command;

        Log.d("CHATBOT SETTINGS", cmd);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                switch (cmd) {

                    case "clear all":
                        db.mWatsonResponseDaoDao().nukeTable();
                        db.mChatMessageDao().nukeTable();
                        messageArrayList.clear();
                        mAdapter.notifyDataSetChanged();
                        Toast.makeText(getActivity(), "Cleared All", Toast.LENGTH_SHORT).show();
                        break;

                    case "clear documents":
                        db.mWatsonResponseDaoDao().nukeTable();
                        Toast.makeText(getActivity(), "Documents Cache cleared", Toast.LENGTH_SHORT).show();
                        break;

                    case "clear messages":
                        db.mChatMessageDao().nukeTable();
                        messageArrayList.clear();
                        mAdapter.notifyDataSetChanged();
                        Toast.makeText(getActivity(), "Messages Cache cleared", Toast.LENGTH_SHORT).show();
                        break;


                    case "increase font size":
                        settings.incrementFontLevel();
                        break;

                    case "decrease font size":
                        settings.decrementFontLevel();
                        break;

                    case "toggle automatic Text-To-Speech":
                        settings.toggleAutoTts();
                        break;

                    case "restore default settings":
                        if (settings.getAutoTts()) {settings.toggleAutoTts();}
                        settings.updateFontLevel(1);
                        break;


                    default:
                        Log.d("CHATBOT SETTINGS", "Unknown settings command: " + cmd);
                        break;
                }

            }
        });

    }
}
