package edutechnologic.nlds.vmac.WatBot;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Liz on 3/31/2018.
 */

@Entity
class ChatMessage {
    @PrimaryKey(autoGenerate = true)
    private int uid;

    @ColumnInfo(name = "message")
    private String message;

    @ColumnInfo(name = "origin_id")
    private Integer origin_id;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getOrigin_id() {
        return origin_id;
    }

    public void setOrigin_id(Integer origin_id) {
        this.origin_id = origin_id;
    }
}
