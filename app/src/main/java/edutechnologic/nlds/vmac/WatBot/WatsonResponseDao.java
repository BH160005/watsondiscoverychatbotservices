package edutechnologic.nlds.vmac.WatBot;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by Liz on 2/22/2018.
 */

@Dao
public interface WatsonResponseDao {
    @Query("SELECT * FROM watsonresponse")
    List<WatsonResponse> getAll();

    @Query("SELECT * FROM watsonresponse WHERE uid IN (:userIds)")
    List<WatsonResponse> loadAllByIds(int[] userIds);

    @Query("SELECT * FROM watsonresponse WHERE user_query LIKE :query")
    WatsonResponse findByName(String query);

    @Insert
    void insertAll(WatsonResponse... responses);

    @Delete
    void delete(WatsonResponse response);

    @Query("DELETE FROM watsonresponse")
    public void nukeTable();
}