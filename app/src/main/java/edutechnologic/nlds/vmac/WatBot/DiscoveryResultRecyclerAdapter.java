package edutechnologic.nlds.vmac.WatBot;

import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Discovery Results adapter
 *
 * @author Don Herwig
 * @since 2017-09-25
 */
public class DiscoveryResultRecyclerAdapter extends RecyclerView.Adapter<DiscoveryResultRecyclerAdapter.ViewHolder>{

    private ArrayList<DiscoveryResult> results;
    private ResultClickListener clickListener;

    /**
     * The method DiscoveryResultRecyclerAdapter keeps track of returned documents
     *
     * @param results - an array containing DiscoveryResult objects that hold results from query
     * @param clickListener - listner for the specific result to know which document to open in webview
     * @see DiscoveryResult
     *
     */
    public DiscoveryResultRecyclerAdapter(ArrayList<DiscoveryResult> results, ResultClickListener clickListener){
        this.results = results;
        this.clickListener = clickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.discovery_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindResult(results.get(position));
        holder.setValues();
        holder.text.setTextSize(TypedValue.COMPLEX_UNIT_SP, Settings.message_font_size);
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    /**
     * Replaces the current adapter data with new data
     * @param newData the new list of data to replace the adapter data with
     */
    public void updateData(ArrayList<DiscoveryResult> newData){
        this.results.clear();
        this.results.addAll(newData);
        notifyDataSetChanged();
    }

    /**
     * A class that holds the view for each recyclerview item
     */
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView text;

        private DiscoveryResult result;

        /**
         * Holds view for if document is clicked on in Dashboard
         *
         * @param itemView HTML to be displayed in webview
         */
        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            text = itemView.findViewById(R.id.text);
        }

        /**
         * Binds the DiscoveryResult in this view with an item in the adapter data list
         * @param result result from discovery
         */
        public void bindResult(DiscoveryResult result){
            this.result = result;
        }

        /**
         * Updates the view of this item based on the Discoveryresult
         */
        public void setValues(){
            String displayText = result.getExtracted_metadata().getTitle();
            text.setText(displayText);
        }


        @Override
        public void onClick(View view) {
            if(clickListener != null){
                clickListener.onClick(result);
            }
        }
    }

    /**
     * A click interface for items in the recyclerview
     */
    public interface ResultClickListener {
        void onClick(DiscoveryResult DiscoveryResult);
    }
}
