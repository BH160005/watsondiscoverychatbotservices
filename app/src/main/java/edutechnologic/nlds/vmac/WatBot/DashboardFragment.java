package edutechnologic.nlds.vmac.WatBot;


import android.app.FragmentTransaction;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * The class DashboardFragment displays the results in a dashboard view on the side of the screen
 *
 * @author Elijah Craig
 * @since 2017-10-22
 */
public class DashboardFragment extends Fragment {


//    public static int discovery_font_size;
//    final private int discovery_minimum_font_size = 10;
    static RecyclerView list;
    private final String DATABASE_NAME = "discovery-cache";
    View CONTENT_VIEW;
    boolean isOpen;
    private ArrayList<DiscoveryResult> LIST_ITEMS;
    private String conversation_response;
    private String user_query;
    private DiscoveryResultRecyclerAdapter adapter;

    public DashboardFragment(){ }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        CONTENT_VIEW = inflater.inflate(R.layout.fragment_dashboard, container, false);
        /*RecyclerView rv = getRecycler();
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        RankAndRetrievalResultRecyclerAdapter ad = new RankAndRetrievalResultRecyclerAdapter(new ArrayList<Document>(), new RankAndRetrievalResultRecyclerAdapter.DocumentClickListener() {
            @Override
            public void onClick(Document document) {

            }
        });
        rv.setAdapter(ad);*/



        LIST_ITEMS = new ArrayList<>();
        RecyclerView list = CONTENT_VIEW.findViewById(R.id.list_items);
        list.setLayoutManager(new LinearLayoutManager(getActivity()));
        initListItems();

        getExpandMenuView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.w("CLICK", "Clicked on expand");
                expandDashboard();
            }
        });
        getCollapseMenuView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.w("CLICK", "Clicked on expand");
                collapseDashboard();
            }
        });


        return CONTENT_VIEW;
    }

    public static void updateAdapter()
    {
        if(list != null) list.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onStart(){
        super.onStart();
        collapseDashboard();
    }

    public ArrayList<DiscoveryResult> getListItems(){
        return this.LIST_ITEMS;
    }

    private View getToggleMenuView(){ return CONTENT_VIEW.findViewById(R.id.menu_toggle); }

    private View getExpandMenuView(){ return CONTENT_VIEW.findViewById(R.id.menu_expand); }

    private View getCollapseMenuView(){ return CONTENT_VIEW.findViewById(R.id.menu_collapse); }

    private View getItemsView(){ return CONTENT_VIEW.findViewById(R.id.list_items); }

    private View getItemsWrapper(){ return CONTENT_VIEW.findViewById(R.id.list_wrapper); }

    private void initListItems(){
        list = CONTENT_VIEW.findViewById(R.id.list_items);
        adapter = new DiscoveryResultRecyclerAdapter(getListItems(), new DiscoveryResultRecyclerAdapter.ResultClickListener() {
            @Override
            public void onClick(DiscoveryResult result) {

                    File folderPath = new File(Environment.getExternalStorageDirectory(), "Download");
                    File url = new File(folderPath, result.getExtracted_metadata().getFilename());
                    addToCache(result, url.toString());

                    if(url.exists())
                    {
                        FileOpener fileOpener = new FileOpener();
                        try {
                            fileOpener.openFile(getActivity(), url);
                        } catch (IOException e) {
                            // if app for opening file doesn't exist
                            Intent intent = new Intent(getActivity(), WebViewActivity.class);
                            intent.putExtra(Constants.Intents.HTML, result.getHtml());
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("result", result);

                            startActivity(intent);
                        }


                    }
                    else
                    {
                        Intent intent = new Intent(getActivity(), WebViewActivity.class);
                        intent.putExtra(Constants.Intents.HTML, result.getHtml());
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("result", result);
                        startActivity(intent);
                    }

            }
        });
        if(list.getAdapter() == null)
            list.setAdapter(adapter);
        else
            list.swapAdapter(adapter, false);


    }

    public void addToCache(DiscoveryResult result, String file_path){

        AppDatabase db = Room.databaseBuilder(getActivity().getApplicationContext(), AppDatabase.class, DATABASE_NAME).allowMainThreadQueries().build();

        WatsonResponse toAdd = new WatsonResponse();
        toAdd.setUser_query(user_query);
        toAdd.setConversation_response(conversation_response);
        toAdd.setDocument_text(result.getText());
        toAdd.setHtml(result.getHtml());
        toAdd.setTitle(result.getExtracted_metadata().getTitle());
        toAdd.setFile_name(file_path);

        db.mWatsonResponseDaoDao().insertAll(toAdd);
    }

    /**
     * The method expndDashboard expands the dashboard view containing results
     */
    public void expandDashboard(){
        getActivity().findViewById(R.id.conversation_display).setVisibility(View.INVISIBLE);
        getExpandMenuView().setVisibility(View.GONE);
        getCollapseMenuView().setVisibility(View.VISIBLE);
        getItemsWrapper().getLayoutParams().height = 0;
        ConstraintSet cs = new ConstraintSet();
        cs.clone((ConstraintLayout)CONTENT_VIEW);
        cs.connect(getToggleMenuView().getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP, 0);
        cs.connect(getItemsWrapper().getId(), ConstraintSet.TOP, getToggleMenuView().getId(), ConstraintSet.BOTTOM, 0);
        cs.applyTo((ConstraintLayout)CONTENT_VIEW);
        getItemsWrapper().setVisibility(View.VISIBLE);
        if(getListItems() != null)
            initListItems();

        isOpen = true;
    }

    /**
     * The method collapseDshboard collapses the dashboard menu containing the results
     */
    public void collapseDashboard(){
        getActivity().findViewById(R.id.conversation_display).setVisibility(View.VISIBLE);
        getItemsWrapper().setVisibility(View.GONE);
        getCollapseMenuView().setVisibility(View.GONE);
        getExpandMenuView().setVisibility(View.VISIBLE);
        ConstraintSet cs = new ConstraintSet();
        cs.clone((ConstraintLayout)CONTENT_VIEW);
        cs.clear(getItemsWrapper().getId(), ConstraintSet.TOP);
        cs.clear(getToggleMenuView().getId(), ConstraintSet.TOP);
        cs.applyTo((ConstraintLayout)CONTENT_VIEW);
        getItemsWrapper().getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;

        isOpen = false;
    }

    public void putArguments(String cr, String query){
        this.conversation_response = cr;
        this.user_query = query;
    }

    /**
     * The method setResult displays results in dashboard
     *
     * @param result a DiscoveryResponse object containing information about result
     * @see DiscoveryResponse
     */
    public void setResult(DiscoveryResponse result){
        this.LIST_ITEMS = result.getResults();
    }

}
