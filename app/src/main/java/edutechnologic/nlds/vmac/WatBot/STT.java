package edutechnologic.nlds.vmac.WatBot;

import android.content.Context;

import com.ibm.watson.developer_cloud.speech_to_text.v1.SpeechToText;

/**
 * Created by seannemann on 2/20/2018.
 */

public class STT implements android.speech.tts.TextToSpeech.OnInitListener {

    private SpeechToText speechToText;
    //private android.speech.tts.TextToSpeech offlineTextToSpeech;


    private final int REQ_CODE_SPEECH_INPUT = 100;


    private String STT_username;
    private String STT_password;
    private NetworkUtil networkUtil;


    public STT(Context context)
    {
        networkUtil = new NetworkUtil(context);

        // offline Speech to Text
        //offlineTextToSpeech = new android.speech.tts.TextToSpeech(context, this);



        //Watson Speech-to-Text Service on Bluemix
        STT_username = context.getString(R.string.STT_username);
        STT_password = context.getString(R.string.STT_password);
        speechToText = new SpeechToText();
        speechToText.setUsernameAndPassword(STT_username, STT_password);


    }


    public boolean checkInternetConnection()
    {
        return networkUtil.checkInternetConnection();

    }


    public  SpeechToText getWatsonService(){
        return speechToText;
    }








    @Override
    public void onInit(int i) {

    }
}
