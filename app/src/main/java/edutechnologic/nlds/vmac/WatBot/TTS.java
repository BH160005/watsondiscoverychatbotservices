package edutechnologic.nlds.vmac.WatBot;

import android.content.Context;

import com.ibm.watson.developer_cloud.android.library.audio.StreamPlayer;
import com.ibm.watson.developer_cloud.text_to_speech.v1.TextToSpeech;
import com.ibm.watson.developer_cloud.text_to_speech.v1.model.Voice;

/**
 * Created by seannemann on 2/20/2018.
 * Class is a wrapper for text to speech, so online and offline text to speech can be handled
 * by the same object
 */

public class TTS implements android.speech.tts.TextToSpeech.OnInitListener {

    private TextToSpeech textToSpeech;
    private android.speech.tts.TextToSpeech offlineTextToSpeech;

    private String TTS_username;
    private String TTS_password;
    private NetworkUtil networkUtil;

    public TTS(Context context)
    {
        networkUtil = new NetworkUtil(context);

        // offline Text to Speech
        offlineTextToSpeech = new android.speech.tts.TextToSpeech(context, this);

        //Watson Text-to-Speech Service on Bluemix
        TTS_username = context.getString(R.string.TTS_username);
        TTS_password = context.getString(R.string.TTS_password);
        textToSpeech = new TextToSpeech();
        textToSpeech.setUsernameAndPassword(TTS_username, TTS_password);

    }

    /**
     * Speaks message out loud
     * @param message message to be spoken
     */
    public void speak(String message)
    {
        if(networkUtil.checkInternetConnection())
        {
            onlineSpeak(message);
        }
        else
        {
            offlineSpeak(message);
        }
    }

    /**
     * Handles offline speech to text
     * @param message message to be spoken
     */
    private void offlineSpeak(String message)
    {
        offlineTextToSpeech.speak(message, android.speech.tts.TextToSpeech.QUEUE_FLUSH, null, null);
    }

    /**
     * Handles online speech to text
     * @param message message to be spoken
     */
    private void onlineSpeak(String message)
    {
        try {
            StreamPlayer streamPlayer = new StreamPlayer();
            streamPlayer.playStream(textToSpeech.synthesize(message, Voice.EN_LISA).execute());
        }
        catch(Exception e)
        {
            offlineSpeak(message);
            e.printStackTrace();
        }
    }

    @Override
    public void onInit(int i) {

    }

    /**
     * Closes speech to text services
     */
    protected void destroyService(){
        offlineTextToSpeech.stop();
        offlineTextToSpeech.shutdown();
    }
}
